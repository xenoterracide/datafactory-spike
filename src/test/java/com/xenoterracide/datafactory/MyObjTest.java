package com.xenoterracide.datafactory;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Before;
import org.junit.Test;

import static com.xenoterracide.datafactory.MyObj.log;

public class MyObjTest {

    DataFactory dataFactory;

    @Before
    public void setup() {
        dataFactory = new DataFactory();
    }

    @Test
    public void testMyObj() {
        for ( int i = 0; i < 20; i++ ) {
            log( dataFactory.getEmailAddress() );
            log( dataFactory.getAddressLine2());
            log( dataFactory.getRandomText( 0, 255 ));
            log( dataFactory.getName());
            log( dataFactory.getCity());
        }
    }

}
