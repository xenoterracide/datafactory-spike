package com.xenoterracide.datafactory;

import org.slf4j.LoggerFactory;

public class MyObj {

    public static void log( final String s ) {
        LoggerFactory.getLogger( MyObj.class ).info( s );
    }
}
